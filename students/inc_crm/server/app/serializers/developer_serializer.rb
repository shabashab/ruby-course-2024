class DeveloperSerializer < ActiveModel::Serializer
  attributes :id, :name, :email
end
