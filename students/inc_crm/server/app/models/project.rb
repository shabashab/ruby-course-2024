class Project < ApplicationRecord
  belongs_to :user, optional: false
  belongs_to :customer, optional: false

  has_many :developer_on_projects
  has_many :developers, through: :developer_on_projects
end
