class User < ApplicationRecord
  has_secure_password
  validates :email, uniqueness: true
  has_many :projects, dependent: :destroy

  enum role: [:user, :admin]
end
