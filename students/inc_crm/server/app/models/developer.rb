class Developer < ApplicationRecord
  has_many :developer_on_projects
  has_many :projects, through: :developer_on_projects
end
