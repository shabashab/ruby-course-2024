class ApplicationController < ActionController::API
  before_action :authenticated

  # TODO: Change to loading from rails secrets
  JWT_ENCRYPTION_KEY = 'jwt_encode_key'

  def encode_token(payload)
    JWT.encode(payload, JWT_ENCRYPTION_KEY)
  end

  def decode_token
    header = request.headers['Authorization']

    if header
      token = header.split(' ')[1]
      begin
        JWT.decode(token, JWT_ENCRYPTION_KEY)
      rescue JWT::DecodeError
        nil
      end
    end
  end

  def current_user
    decoded_token = decode_token

    return nil unless !!decoded_token

    user_id = decoded_token[0]['user_id']
    @authenticated_user = User.find_by(id: user_id)

    @authenticated_user
  end

  def authenticated
    unless current_user
      render json: { message: 'Unauthorized' }, status: :unauthorized
    end
  end

  def authorized_admin
    authenticated

    unless current_user.admin?
      render json: { message: 'Forbidden' }, status: :forbidden
    end
  end
end
