class AuthController < ApplicationController
  wrap_parameters false
  skip_before_action :authenticated, only: [:login]

  def login
    params = login_params
    user = User.find_by(email: params[:email])

    unless user
      render json: {
        message: 'Unauthorized'
      }, status: :unauthorized
      return
    end

    if user.authenticate(params[:password])
      token = encode_token(user_id: user.id)
      render json: {
        user: UserSerializer.new(user),
        # user: user,
        token: token
      }
      return
    end

    render json: {
      message: 'Unauthorized'
    }, status: :unauthorized
  end

  def iam
    render json: @authenticated_user
  end

  private

  def login_params
    params.permit(:email, :password)
  end
end
