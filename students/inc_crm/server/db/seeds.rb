# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

admin_user = User.find_by(email: 'admin@admin.com')
unless admin_user
  User.create(
    email: 'admin@admin.com',
    name: 'Admin',
    password: 'password',
    role: :admin
  )
end

customer_1 = Customer.find_by(email: 'customer1@external.com')
begin
  customer_1 = Customer.create(
    name: 'Customer 1',
    email: 'customer1@external.com'
  )
end

customer_2 = Customer.find_by(email: 'customer1@external.com')
begin
  customer_2 = Customer.create(
    name: 'Customer 2',
    email: 'customer2@external.com'
  )
end

customer_2 = Customer.find_by(email: 'customer2@external.com')
begin

end

begin
  user = User.find_by(email: 'ben@inc-dev.com')
  unless user
    user = User.create(
      email: 'ben@inc-dev.com',
      name: 'Ben',
      role: :user,
      password: 'password'
    )
  end

  begin
    user_project = user.projects.find_by(name: 'Big Informational System')
    unless user_project
      user.projects.create(
        name: 'Big Informational System',
        customer_id: customer_1.id
      )
    end
  end

  begin
    user_project = user.projects.find_by(name: 'Small CRM')
    unless user_project
      user.projects.create(
        name: 'Small CRM',
        customer_id: customer_2.id
      )
    end
  end
end


begin
  user = User.find_by(email: 'dan@inc-dev.com')
  unless user
    user = User.create(
      email: 'dan@inc-dev.com',
      name: 'Dan',
      role: :user,
      password: 'password'
    )
  end

  begin
    user_project = user.projects.find_by(name: 'Web3 World')
    unless user_project
      user.projects.create(
        name: 'Web3 World',
        customer_id: customer_2.id
      )
    end
  end

  begin
    user_project = user.projects.find_by(name: 'MegaCOIN Swap')
    unless user_project
      user.projects.create(
        name: 'MegaCOIN Swap',
        customer_id: customer_1.id
      )
    end
  end
end

begin
  user = User.find_by(email: 'john@inc-dev.com')
  unless user
    User.create(
      email: 'john@inc-dev.com',
      name: 'John',
      role: :user,
      password: 'password'
    )
  end
end
