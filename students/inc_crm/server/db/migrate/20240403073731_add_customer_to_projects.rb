class AddCustomerToProjects < ActiveRecord::Migration[7.1]
  def change
    add_belongs_to :projects, :customer, foreign_key: true, null: false
  end
end
