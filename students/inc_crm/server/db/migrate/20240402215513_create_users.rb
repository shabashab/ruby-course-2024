class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :email, index: { unique: true, name: 'unique_emails' }
      t.string :name, null: false
      t.string :password_digest
      t.column :role, :integer, default: 0


      t.timestamps
    end
  end
end
