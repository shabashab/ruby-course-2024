class CreateDeveloperOnProjects < ActiveRecord::Migration[7.1]
  def change
    create_table :developer_on_projects do |t|
      t.belongs_to :developer, foreign_key: true, null: false
      t.belongs_to :project, foreign_key: true, null: false

      t.timestamps
    end
  end
end
