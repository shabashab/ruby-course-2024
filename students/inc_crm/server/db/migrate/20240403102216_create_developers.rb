class CreateDevelopers < ActiveRecord::Migration[7.1]
  def change
    create_table :developers do |t|
      t.string :name
      t.string :email, index: { unique: true, name: 'unique_developer_emails' }

      t.timestamps
    end
  end
end
