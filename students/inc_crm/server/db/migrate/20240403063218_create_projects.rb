class CreateProjects < ActiveRecord::Migration[7.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.belongs_to :user, foreign_key: true, null: false

      t.timestamps
    end
  end
end
