class CreateCustomers < ActiveRecord::Migration[7.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email, index: { unique: true, name: 'unique_customers_emails' }

      t.timestamps
    end
  end
end
