export default defineNuxtRouteMiddleware((to) => {
  if (to.meta.ignoreAuth) return Promise.resolve();

  return new Promise((resolve, _reject) => {
    const authStore = useAuthStore();

    const checkAuthentication = () => {
      if (
        !authStore.isAuthenticated
      ) {
        resolve({
          path: "/auth/login",
        });
      } else {
        resolve();
      }
    };

    checkAuthentication();

    // if (!authStore.fetchedState) {
    //   whenever(
    //     () => authStore.fetchedState,
    //     () => {
    //       checkAuthentication();
    //     },
    //   );
    // } else checkAuthentication();
  });
});