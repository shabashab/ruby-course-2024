// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: [
    'primevue/resources/themes/md-light-indigo/theme.css',
    'primeicons/primeicons.css',
    "primevue/resources/primevue.min.css",
  ],
  modules: ['@nuxtjs/tailwindcss', '@pinia/nuxt', 'nuxt-icon', '@vueuse/nuxt'],
  imports: {
    dirs: ["utils", "composable", "api", "store", "rules", "models", "providers", "api/dto"],
  },
  build: {
    transpile: ["primevue"],
  },
})