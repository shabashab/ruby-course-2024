export interface CreateUserDto extends User {
  password: string,
  confirmPassword: string,
}