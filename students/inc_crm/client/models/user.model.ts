export type UserRole = "admin" | "manager";
export interface User {
  email: string,
  name: string,
  role: UserRole
}