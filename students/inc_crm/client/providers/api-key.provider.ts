const API_KEY_LOCAL_STORAGE_KEY = "api_key";

export const getApiKey = (): string | undefined => {
  if (import.meta.env.SSR) return undefined;

  const key = localStorage.getItem(API_KEY_LOCAL_STORAGE_KEY);

  if (!key) return undefined;

  return key;
};

export const setApiKey = (value: string) => {
  if (import.meta.env.SSR) return;

  localStorage.setItem(API_KEY_LOCAL_STORAGE_KEY, value);
};

export const removeApiKey = () => {
  if (import.meta.env.SSR) return;

  localStorage.removeItem(API_KEY_LOCAL_STORAGE_KEY);
};