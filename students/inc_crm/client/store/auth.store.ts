export const useAuthStore = defineStore('auth-store', () => {
  const user = ref<User | null>(
    {
      email: "admin@admin.com",
      name: "Admin Adminovich",
      role: "admin"
    }
  )

  const isAuthenticated = computed(() => user.value !== null)

  const fetchedState = ref(false);

  const login = async (email: string, password: string) => {
      user.value = {
        email,
        name: "Admin Adminovich",
        role: "admin"
      }

      setApiKey("token")
  }

  const logout =  async () => {
    user.value = null
    removeApiKey()
  }

  return {
    user,
    isAuthenticated,
    login,
    logout,
    fetchedState
  }
})