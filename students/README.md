# Project Requirements & Info

### Команди
* Команда має складатися з однієї людини (для інтровертів), з двох, трьох людей
* Придумайте назву своїй команді

### Огранізація
* Зробіть clone репозиторію (якщо ви цього досі не зробили) - `git clone https://gitlab.com/pavlozahozhenko/ruby-course-2024.git`
* Створіть нову гілку з назвою `назва_команди_project` і додайте її до репозиторію. Наприклад:
```
git checkout -b example_team_project
mkdir students/example_team
touch students/example_team/.gitkeep
git add students/example_team/
git commit --message="Added project folder"
git push origin example_team_project
```
* Працюйте над своїм проектом в цій гілці у відповідній теці (students/назва_команди), додавайте коміти
* Коли проект буде готовий - зробіть merge request в бранч master, щоб я зробив code review

### Вимоги до проекта
Проект на вільну тему, але з деякими обмеженнями, а саме:

#### Обов'язкові
* Будь-який Rack framework на Ruby: Ruby on Rails, Sinatra, Roda, Hanami, Cuba, Padrino, etc. За використання не-Rails Rack framework-a (Sinatra, Roda etc) - +10 балів
* Аутентифікація користувачів
* Авторизація, мінімум 2 ролі користувачів з різними правами доступу
* Елементи клієнт-серверної взаємодії зроблені за допомогою Hotwire/Turbo (Rails defailt). Або full SPA на якомусь із JS frameworks
* Хоча б одна задача, яка виконується в фоновому (background) режимі (тобто асинхронно в окремому процесі; за допомогою Sidekiq або аналогів)
* 2+ локалі, не використовувати hardcoded тексти
* Автоматизоване тестування. Unit-тести і acceptance-тести для main flow

#### Would be a plus
- Функціональні (integration) тести (тести контроллерів), тести background jobs. Full test coverage according to simplecov
- Підготовка для запуску в production середовищі, використання production-ready СКБД, e.g. PostgreSQL. "production" environment
- Взаємодія сервер-клієнт через websockets (ActionCable або Turbo Stream)
- Inbound SMTP server (отримання та процессінґ імейлів) via ActionMailbox * (задача з зірочкою)

### Milestones
- "MVP" - 3.04.2024:
  - проект, предметна область
  - команда
  - модель/схема даних з 1-1, 1-n, n-m асоціаціями
  - мінімум 1 CRUD
  - аутентифікація
  - авторизація
- "Release" - 17.04.2024: full project requirements implementation

### Загальна інформація
* Тему проекта бажано узгодити зі мною; можу підказати якісь архітектурні рішення, чи варто використати якісь gems і т.д.
* Якщо у вас брак ідей щодо теми проекта - звертайтеся, щось придумаємо
