def do_smth(a, custom_proc)
  puts 'hello!'
  b = a * 2
  res = yield(b) if block_given?
  res += custom_proc.call(res)
  res *= 2
  puts "world #{res}"
end
