module Biology
  class Animal
  end
end

class Biology::Human < Biology::Animal
end

module Util
  def self.do_smth
    puts 'Doing something...'
  end
end

class Human
  CONST = 10
  attr_reader :weight
  attr_accessor :age

  [:foo, :baz, :bar].each do |method_name|
    define_method method_name do
      puts method_name
    end
  end

  def initialize(age = 0, weight:1)
    @@count ||= 0
    @@count += 1
    @age = age
    @weight = weight
    age_in_init = age
    puts 'initialize method called...'
  end

  def print_age
    puts "Self is #{self}"
    puts "My age is #{@age} year(s)"
    @age + 1
    self.class.define_method "age_#{@age}" do
      puts "it's a wonderful age"
    end
  end

  def self.call_static_method
    puts 'foo bar'
  end

  def num_of_objects
    @@count
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /foobar\d/
      puts 'foobar implementation goes here'
    else
      super
    end
  end
end

module UniversityMixin
  def visit_university
    puts 'Going there....'
  end
end

class Student < Human
  include UniversityMixin

  def initialize(age:, university: 'NaUKMA')
    puts 'init in student'
    super(age)
    @university = university
  end

  def print_age
    res = super
    puts "super method call returned #{res}"
  end
end

class Teacher < Human
  include UniversityMixin
end

class FinStudent < Student
end
