FactoryBot.define do
  factory :message do
    text { FFaker::Lorem.paragraph[0, 200] }
    user
  end
end
