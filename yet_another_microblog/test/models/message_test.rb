require "test_helper"

class MessageTest < ActiveSupport::TestCase
  def subject
    @message ||= Message.new(text: 'foo bar', user: users(:testman))   
  end

  test "length is 0 for the empty message" do
    subject.text = ''
    assert_equal subject.length, 0
  end

  test "length is 3 for the message 'foo'" do
    subject.text = 'foo'
    assert_equal subject.length, 3
  end
end
