class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :name, null: false, limit: 250
      t.string :email, null: false, limit: 100
      t.timestamps
    end
  end
end
