class CreateFollowings < ActiveRecord::Migration[7.1]
  def change
    create_table :followings do |t|
      t.references :user
      t.integer :follower_id, null: false
      t.timestamps
    end
  end
end
