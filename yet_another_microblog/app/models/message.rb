class Message < ApplicationRecord
  belongs_to :user

  validates :text, presence: true, length: { in: 5..256 }

  after_create do |message|
    GatherStatsJob.perform_later(message.id)
  end

  def gather_stats
    puts 'gathering stats...'
    sleep 10
    puts 'done!'
  end

  def length
    text.size
  end
end
