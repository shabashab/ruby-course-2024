Given /^user exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com'
end

Given /^admin exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com', is_admin: true
end

Given /^user is logged in$/ do
  login_as(@user)
end

Given /^user is an admin$/ do
  @user.update_column(:is_admin, true)
end

Given /the user exists with email "(.*)"/ do |email|
  ::FactoryBot.create(:user, email: email)
end

Then /there should exist a user with the name "(.*)"/ do |full_name|
  user = ::User.find_by(name: full_name)
  assert_not_nil(user)
end

Then /there should be a message with the text "(.*)"/ do |text|
  @message = ::Message.find_by(text: text)
  assert_not_nil(@message)
end
