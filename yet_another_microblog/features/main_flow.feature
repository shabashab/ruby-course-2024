Feature: Main flow

  Background:
    Given user exists
    And I am on login page
    When I fill in "user[email]" field with "test@example.com"
    And I fill in "user[password]" field with "password"
    And I click on "Sign in"

  @javascript
  Scenario: user logs into the system
    Then I should see text "Hello, world!"

  @javascript
  Scenario: User logs into the system and adds a new message
    Given user is an admin
    When I go to messages
    And I wait for 2 seconds
    And I fill in "message[text]" with "Lorem ipsum"
    And I click on "Create Message"
    Then I should see text "Lorem ipsum"
    And there should be a message with the text "Lorem ipsum"
    And there should be 1 message on the page
